import math
import time
from laser_tank import LaserTankMap


"""
Assignment 3 Value Iteration Example Solution
COMP3702/7702 2020

Last updated by njc 14/11/20
"""


class Solver:

    GET_LEFT_DIR = {LaserTankMap.UP: LaserTankMap.LEFT,
                    LaserTankMap.LEFT: LaserTankMap.DOWN,
                    LaserTankMap.DOWN: LaserTankMap.RIGHT,
                    LaserTankMap.RIGHT: LaserTankMap.UP}
    GET_RIGHT_DIR = {LaserTankMap.UP: LaserTankMap.RIGHT,
                     LaserTankMap.RIGHT: LaserTankMap.DOWN,
                     LaserTankMap.DOWN: LaserTankMap.LEFT,
                     LaserTankMap.LEFT: LaserTankMap.UP}

    def __init__(self, game_map):
        self.game_map = game_map
        teleports = []
        for x in range(game_map.x_size):
            for y in range(game_map.y_size):
                if game_map.grid_data[y][x] == LaserTankMap.TELEPORT_SYMBOL:
                    teleports.append((x, y))
        self.teleports = teleports
        # VI and PI tables
        self.values = None
        self.policy = None

    def run_value_iteration(self):
        """
        Do Value Iteration offline planning
        :return: (values, policy)
        """
        game_map = self.game_map
        teleports = self.teleports

        values = [[[0 for _ in LaserTankMap.DIRECTIONS]
                   for __ in range(1, game_map.y_size - 1)]
                  for ___ in range(1, game_map.x_size - 1)]
        policy = [[[-1 for _ in LaserTankMap.DIRECTIONS]
                   for __ in range(1, game_map.y_size - 1)]
                  for ___ in range(1, game_map.x_size - 1)]

        iterations = 0
        t0 = time.time()
        while True:
            max_delta = 0
            for x in range(game_map.x_size - 2):
                for y in range(game_map.y_size - 2):
                    for d in LaserTankMap.DIRECTIONS:
                        if x + 1 == game_map.flag_x and y + 1 == game_map.flag_y:
                            values[x][y][d] = game_map.goal_reward
                            continue
                        elif game_map.grid_data[y + 1][x + 1] == LaserTankMap.WATER_SYMBOL:
                            values[x][y][d] = game_map.game_over_cost
                            continue
                        elif game_map.grid_data[y + 1][x + 1] == LaserTankMap.OBSTACLE_SYMBOL:
                            continue

                        # store old values for epsilon check
                        old_val = values[x][y][d]

                        # for each move, cost = collision penalty + expected next value
                        q_left = game_map.move_cost + (game_map.gamma * values[x][y][self.GET_LEFT_DIR[d]])
                        q_right = game_map.move_cost + (game_map.gamma * values[x][y][self.GET_RIGHT_DIR[d]])
                        q_forward = 0
                        for forward in [0, 1]:
                            for sideways in [-1, 0, 1]:
                                if forward == 1 and sideways == 0:
                                    t_prob = game_map.t_success_prob
                                else:
                                    t_prob = game_map.t_error_prob / 5
                                # apply basic transition
                                nx, ny = get_position_in_direction(x, y, d, forward, sideways)
                                if game_map.grid_data[ny + 1][nx + 1] == LaserTankMap.OBSTACLE_SYMBOL:
                                    q_forward += t_prob * (game_map.collision_cost + (game_map.gamma * values[x][y][d]))
                                    continue
                                # slide along ice
                                while game_map.grid_data[ny + 1][nx + 1] == LaserTankMap.ICE_SYMBOL:
                                    tnx, tny = get_position_in_direction(nx, ny, d, 1, 0)
                                    if game_map.grid_data[tny + 1][tnx + 1] != LaserTankMap.OBSTACLE_SYMBOL:
                                        nx, ny = tnx, tny
                                    else:
                                        break
                                # travel through teleport
                                if game_map.grid_data[ny + 1][nx + 1] == LaserTankMap.TELEPORT_SYMBOL:
                                    if (nx, ny) == teleports[0]:
                                        nx, ny = teleports[1]
                                    else:
                                        nx, ny = teleports[0]
                                q_forward += t_prob * (game_map.move_cost + (game_map.gamma * values[nx][ny][d]))
                        # choose best q value
                        best_q = -math.inf
                        best_a = None
                        for q, a in [(q_left, LaserTankMap.TURN_LEFT),
                                     (q_right, LaserTankMap.TURN_RIGHT),
                                     (q_forward, LaserTankMap.MOVE_FORWARD)]:
                            if q > best_q:
                                best_q = q
                                best_a = a

                        # store result
                        values[x][y][d] = best_q
                        policy[x][y][d] = best_a

                        # update delta
                        if abs(old_val - values[x][y][d]) > max_delta:
                            max_delta = abs(old_val - values[x][y][d])

            iterations += 1

            # check terminate conditions
            if max_delta < game_map.epsilon or time.time() - t0 > game_map.time_limit - 1:
                break
        self.values = values
        self.policy = policy

    def run_policy_iteration(self):
        """
        Do Policy Iteration offline planning
        :return: (values, policy)
        """
        pass

    def get_offline_value(self, state):
        """
        Get the value of this state
        :param state: a LaserTankMap instance
        :return: V(s)
        """
        x, y, d = extract_state(state)
        return self.values[x][y][d]

    def get_offline_policy(self, state):
        """
        Get the policy for this state (i.e. the action that should be performed at this state
        :param state: a LaserTankMap instance
        :return: pi(s) [an element of LaserTankMap.MOVES]
        """
        x, y, d = extract_state(state)
        return self.policy[x][y][d]

    def get_mcts_policy(self, state):
        """
        Choose an action to be performed using online MCTS.
        :param state: a LaserTankMap instance
        :return: pi(s) [an element of LaserTankMap.MOVES]
        """
        pass


def get_position_in_direction(x, y, d, forward, sideways):
    if d == LaserTankMap.UP:
        if forward == 1:
            ny = y - 1
        else:
            ny = y

        if sideways == -1:
            nx = x - 1
        elif sideways == 1:
            nx = x + 1
        else:
            nx = x
    elif d == LaserTankMap.DOWN:
        if forward == 1:
            ny = y + 1
        else:
            ny = y

        if sideways == -1:
            nx = x + 1
        elif sideways == 1:
            nx = x - 1
        else:
            nx = x
    elif d == LaserTankMap.LEFT:
        if forward == 1:
            nx = x - 1
        else:
            nx = x

        if sideways == -1:
            ny = y - 1
        elif sideways == 1:
            ny = y + 1
        else:
            ny = y
    else:
        if forward == 1:
            nx = x + 1
        else:
            nx = x

        if sideways == -1:
            ny = y + 1
        elif sideways == 1:
            ny = y - 1
        else:
            ny = y
    return nx, ny


def extract_state(game_map):
    return game_map.player_x - 1, game_map.player_y - 1, game_map.player_heading

